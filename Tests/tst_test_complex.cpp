#include "C:\Users\Roman\Desktop\QT\NewProject\Release\operators.cpp"
#include <QtTest>
// add necessary includes here

class Test_Complex : public QObject
{
    Q_OBJECT
private slots:
    void testPow();
    void testRoot();
    void sum();
    void subtr();
    void test_mult();
    void test_eq();
    void test_div();
    void test_sqr();
    void test_sqrt();
};

void Test_Complex::test_sqrt()
{
    Complex example(2, 2);
    Complex result = squareRoot(example);
    const int pi=3.14159265358979323846;
    QCOMPARE(result.getRe(), floor(sqrt(2*sqrt(2))*cos(pi/8)));
    QCOMPARE(result.getIm(), floor(sqrt(2*sqrt(2))*sin(pi/8)));

}

void Test_Complex::test_sqr()
{
    Complex test1(2,2);
    Complex test2(5,3);
    QCOMPARE(pow2(test1), Complex(0,8));
    QCOMPARE(pow2(test2), Complex(16,30));
}

void Test_Complex::sum()
{
    Complex test1(1,1);
    Complex test2(2,2);
    Complex test3=test1 + test2;
    QCOMPARE(test3.getRe(),3);
    QCOMPARE(test3.getIm(),3);
}

void Test_Complex::subtr()
{
    Complex test1(5,3);
    Complex test2(2,2);
    Complex test3=test1 - test2;
    QCOMPARE(test3.getRe(),3);
    QCOMPARE(test3.getIm(),1);
}


void Test_Complex::test_eq()
{
    Complex test1(1,1);
    Complex test2(1,1);
    Complex test3(2,2);
    QCOMPARE(test1==test2, true);
    QCOMPARE(test1==test3, false);
    QCOMPARE(test2==test1, true);
}

void Test_Complex::test_mult()
{
    Complex a(1,1);
    Complex b(2,2);
    Complex c=a*b;
    Complex d=c*b;
    QCOMPARE(c.getRe(),0);
    QCOMPARE(c.getIm(),4);
    QCOMPARE(d.getRe(), -8);
    QCOMPARE(d.getIm(), 8);
}

void Test_Complex::testPow()
{
    Complex example(2, 2);
    Complex result = powComplex(example , 2);

    QCOMPARE(result.getRe(), 0);
    QCOMPARE(result.getIm(), 8);
}

void Test_Complex::testRoot()
{
    Complex example(2, 2);
    Complex result = rootExtraction(example , 2);
    const int pi=3.14159265358979323846;
    QCOMPARE(result.getRe(), floor(sqrt(2*sqrt(2))*cos(pi/8)));
    QCOMPARE(result.getIm(), floor(sqrt(2*sqrt(2))*sin(pi/8)));
}

void Test_Complex::test_div()
{
    Complex example_first(2, 7);
    Complex example_second(10, 0);
    Complex result = example_first / example_second;
    QCOMPARE(result.getIm(), 0.7);
    QCOMPARE(result.getRe(), 0.2);
}
QTEST_APPLESS_MAIN(Test_Complex)

#include "tst_test_complex.moc"
