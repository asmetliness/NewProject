#include "operators.h"
#include <cmath>

Complex powComplex(Complex left, int n)
{
    double r = pow(sqrt(pow(left.getRe(), 2) + pow(left.getIm(), 2)), n);
    double fi = n*atan2(left.getIm(),left.getRe());
    double re = floor(r * cos(fi));
    double im = floor(r * sin(fi));
    Complex result(re, im);
    return result;
}

Complex rootExtraction(Complex left, int k)
{
    double r = pow(sqrt(pow(left.getRe(), 2) + pow(left.getIm(), 2)), 1/k);
    double fi = atan2(left.getIm(),left.getRe())/k;
    double re = ceil(r * cos(fi));
    double im = floor(r * sin(fi));
    Complex result(re, im);
    return result;
}

Complex squareRoot(Complex left)
{
    return rootExtraction(left, 2);
}

Complex operator +(Complex left, Complex right)
{
    Complex sum(left.getRe() + right.getRe(), left.getIm() + right.getIm());
    return sum;
}

Complex operator -(Complex left, Complex right)
{
    Complex subtraction(left.getRe() - right.getRe(), left.getIm() - right.getIm());
    return subtraction;
}

Complex operator*(Complex left, Complex right)
{
    double re=left.getRe()*right.getRe()-left.getIm()*right.getIm();
    double im=left.getRe()*right.getIm()+left.getIm()*right.getRe();
    Complex tmp(re, im);
    return tmp;
}

bool operator==(Complex left, Complex right)
{
    return ((left.getRe()==right.getRe())&&(left.getIm()==right.getIm()));
}

Complex operator /(Complex first, Complex second)
{
    Complex div(((first.getIm()*second.getIm()) + (first.getRe()*second.getRe()))/((second.getIm()*second.getIm())+(second.getRe()*second.getRe())),
                -((first.getRe()*second.getIm())-(first.getIm()*second.getRe()))/((second.getIm()*second.getIm())+(second.getRe()*second.getRe())));
    return div;
}

Complex pow2(Complex com)
{
    return powComplex(com,2);
}

