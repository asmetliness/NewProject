#include "interface.h"

Interface::Interface(QWidget *pwgt):QWidget(pwgt)
{
    QDoubleValidator* validator=new QDoubleValidator;
    QLocale locale(QLocale::English);
    validator->setLocale(locale);

    enterFirstRe=new QLineEdit;
    enterFirstRe->setValidator(validator);
    enterFirstIm=new QLineEdit;
    enterFirstIm->setValidator(validator);

    enterSecondIm=new QLineEdit;
    enterSecondIm->setValidator(validator);
    enterSecondRe=new QLineEdit;
    enterSecondRe->setValidator(validator);

    outPut=new QLineEdit;

    root2=new QPushButton("Извлечь квадратный корень");
    sqr2 = new QPushButton("Возвести в квадрат");
    plus=new QPushButton("+");
    minus=new QPushButton("-");
    mult=new QPushButton("*");
    equal=new QPushButton("==");
    sqr=new QPushButton("Возвести в степень");
    sqrt=new QPushButton("Извлечь корень");
    clear=new QPushButton("Очистить");
    div=new QPushButton("/");

    connect(root2, SIGNAL(clicked(bool)), SLOT(getRoot2()));
    connect(clear, SIGNAL(clicked(bool)), SLOT(clearAll()));
    connect(plus, SIGNAL(clicked(bool)), SLOT(sum()));
    connect(minus, SIGNAL(clicked(bool)), SLOT(min()));
    connect(mult, SIGNAL(clicked(bool)), SLOT(multipl()));
    connect(equal, SIGNAL(clicked(bool)), SLOT(eq()));
    connect(sqr, SIGNAL(clicked(bool)), SLOT(getSqr()));
    connect(sqrt, SIGNAL(clicked(bool)), SLOT(getSqrt()));
    connect(div, SIGNAL(clicked(bool)), SLOT(division()));
    connect(sqr2, SIGNAL(clicked(bool)), this, SLOT(getSqr2()));


    QLabel* lbl1=new QLabel("Введите первое число");
    QLabel* lbl2=new QLabel("Введите второе число");

    QVBoxLayout* lay=new QVBoxLayout;
    lay->addWidget(lbl1);
    QHBoxLayout* layFirst=new QHBoxLayout;
    layFirst->addWidget(enterFirstRe);
    layFirst->addWidget(enterFirstIm);
    lay->addLayout(layFirst);

    lay->addWidget(lbl2);
    QHBoxLayout* laySecond=new QHBoxLayout;
    laySecond->addWidget(enterSecondRe);
    laySecond->addWidget(enterSecondIm);
    lay->addLayout(laySecond);

    QHBoxLayout* layOperators=new QHBoxLayout;
    layOperators->addWidget(plus);
    layOperators->addWidget(minus);
    layOperators->addWidget(mult);
    layOperators->addWidget(equal);
    layOperators->addWidget(div);
    lay->addLayout(layOperators);


    lay->addWidget(root2);
    lay->addWidget(sqr2);
    lay->addWidget(sqr);
    lay->addWidget(sqrt);
    lay->addWidget(clear);
    lay->addWidget(outPut);

    setLayout(lay);
    resize(400,400);
}

void Interface::getRoot2()
{
    Complex tmp(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    result=squareRoot(tmp);
    setOutPut();
}

void Interface::getSqr2()
{
    result = pow2(Complex(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble()));
    setOutPut();
}

void Interface::clearAll()
{
    enterFirstIm->clear();
    enterFirstRe->clear();
    enterSecondIm->clear();
    enterSecondRe->clear();
    outPut->clear();
}

void Interface::sum()
{
    Complex first(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    Complex second(enterSecondRe->text().toDouble(), enterSecondIm->text().toDouble());
    result=first+second;
    setOutPut();
}

void Interface::min()
{
    Complex first(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    Complex second(enterSecondRe->text().toDouble(), enterSecondIm->text().toDouble());
    result=first-second;
    setOutPut();
}

void Interface::multipl()
{
    Complex first(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    Complex second(enterSecondRe->text().toDouble(), enterSecondIm->text().toDouble());
    result=first*second;
    setOutPut();
}

void Interface::eq()
{
    Complex first(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    Complex second(enterSecondRe->text().toDouble(), enterSecondIm->text().toDouble());
    outPut->clear();
    if(first==second)
    {
        outPut->setText("true");
    }
    else
    {
        outPut->setText("false");
    }
}

void Interface::getSqr()
{
    Complex tmp(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    if(enterSecondRe->text().isEmpty())
    {
        return;
    }
    result=powComplex(tmp, enterSecondRe->text().toDouble());
    setOutPut();
}

void Interface::getSqrt()
{
    Complex tmp(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    if(enterSecondRe->text().isEmpty())
    {
        return;
    }
    result=rootExtraction(tmp, enterSecondRe->text().toDouble());
    setOutPut();
}

void Interface::setOutPut()
{
    QString str=QString().setNum(result.getRe());
    if(result.getIm()>=0)
    {
        str+="+";
    }

    str+=QString().setNum(result.getIm())+"*i";
    outPut->clear();
    outPut->setText(str);
}
void Interface::division()
{
    Complex left(enterFirstRe->text().toDouble(), enterFirstIm->text().toDouble());
    Complex right(enterSecondRe->text().toDouble(), enterSecondIm->text().toDouble());
    result=left/right;
    setOutPut();
}
