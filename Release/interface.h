#ifndef INTERFACE_H
#define INTERFACE_H
#include <QtWidgets>
#include"complex.h"
#include "operators.h"
class Interface: public QWidget
{
    Q_OBJECT
    Complex result;
    QLineEdit* enterFirstRe;
    QLineEdit* enterFirstIm;
    QLineEdit* enterSecondRe;
    QLineEdit* enterSecondIm;
    QPushButton* plus;
    QPushButton* minus;
    QPushButton* mult;
    QPushButton* equal;
    QPushButton* sqr;
    QPushButton* sqrt;
    QPushButton* clear;
    QPushButton *root2;
    QPushButton* sqr2;
    QLineEdit *outPut;
    QPushButton *div;
public:
    Interface(QWidget* pwgt=0);
    void setOutPut();
public slots:
    void getSqr2();
    void sum();
    void min();
    void multipl();
    void eq();
    void getSqr();
    void getSqrt();
    void clearAll();
    void getRoot2();
    void division();

};
#endif // INTERFACE_H
