#ifndef OPERATORS_H
#define OPERATORS_H
#include "complex.h"


Complex operator+(Complex left, Complex right);

Complex operator-(Complex left, Complex right);

Complex operator*(Complex left, Complex right);

bool operator==(Complex left, Complex right);

Complex powComplex(Complex left, int n);

Complex rootExtraction(Complex left, int n);

Complex squareRoot(Complex left);

Complex operator/ (Complex first, Complex second);

Complex pow2(Complex com);

#endif // OPERATORS_H
