#ifndef COMPLEX_H
#define COMPLEX_H

class Complex
{
    double re;
    double im;

public:
    Complex(double re=0, double im=0)
    {
        this->re=re;
        this->im=im;
    }

    double getRe()
    {
        return re;
    }

    double getIm()
    {
        return im;
    }

    void setRe(double re)
    {
        this->re=re;
    }

    void setIm(double im)
    {
        this->im=im;
    }
};
#endif // COMPLEX_H
